odoo.define("purchasef99/static/js/purchasef99.js", (require) => {
    const ActionManager = require('web.ActionManager')
    ActionManager.include({
        _handleAction: function(action, options) {
            if(action.type === "ir.actions.client") {
                if(action.tag === "close_dialog_and_refresh") {
                    this._closeDialog()
                    this.getCurrentController().widget.trigger_up("reload", {keepChanges: true})
                    this.getCurrentController().widget.trigger_up("dme-document-action-manager-custom-event-reselect-multiple", {
                        document_ids: action.payload
                    })
                    return Promise.resolve()
                }
                else if(action.tag === "do_nothings") {
                    this.getCurrentController().widget.trigger_up("reload", {keepChanges: true})
                    return Promise.resolve()
                }
            }
            return this._super.apply(this, arguments)
        },
    })
})