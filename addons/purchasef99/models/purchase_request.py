import logging
from odoo import models, fields, api, exceptions
from odoo.tools.translate import _

logger = logging.getLogger(__name__)

class PurchaseRequest(models.Model):
    _inherit = ["mail.thread", "mail.activity.mixin"]
    _name = "purchasef99.purchaserequest"
    _description = _("Purchase request")

    name = fields.Char(string = _("Purchase request"), default = _("New"), required=True, index=True, copy=False)
    inventory_id = fields.Many2one("stock.warehouse", string = _("Inventory"), index=True, copy=False)
    delivery_datetime = fields.Datetime(string = _("Delivery datetime"), index=True, copy=False)
    department_id = fields.Many2one("hr.department", string = _("Department"))
    rfq_ids = fields.Many2many("purchase.order", string = _("Orders"))
    state = fields.Selection([
        ("draft", _("Draft")),
        ("approved", _("Approved")),
        ("rejected", _("Rejected")),
    ], default = "draft", index=True)

    product_line_ids = fields.One2many("purchasef99.purchaserequest.line", inverse_name = "purchaserequest_id",  string = _("Products"))
    currency_id = fields.Many2one("res.currency", _("Currency"), required=True, default=lambda self: self.env.company.currency_id.id)
    company_id = fields.Many2one("res.company", _("Company"), required=True, index=True, default=lambda self: self.env.company.id)

    @api.model
    def create(self, vals):
        company_id = vals.get("company_id", self.default_get(["company_id"])["company_id"])
        self_comp = self.with_company(company_id)
        if vals.get("name", "New") == "New":
            seq_date = None
            vals["name"] = self_comp.env["ir.sequence"].next_by_code("purchasef99.purchaserequest", sequence_date=seq_date) or "/"
        return super(PurchaseRequest, self_comp).create(vals)

    def approve(self):
        self.ensure_one()
        self.state =  "approved"
class PurchaseRequestLine(models.Model):
    _name = "purchasef99.purchaserequest.line"
    _description = _("Purchase Request Line")

    name = fields.Char(related = "product_id.name")
    purchaserequest_id = fields.Many2one("purchasef99.purchaserequest", string="Purchase Request", index=True, required=True, ondelete="cascade")
    product_id = fields.Many2one("product.product", string = _("Product"), domain = [("purchase_ok", "=", True)])
    product_uom_category_id = fields.Many2one(related='product_id.uom_id.category_id')
    product_uom = fields.Many2one('uom.uom', string= _('Unit of Measure'), domain="[('category_id', '=', product_uom_category_id)]", required = True)
    price_unit = fields.Monetary(string = _("Unit Price"), required = True, digits = "Product Price")
    product_qty = fields.Float(string = _("Quantity"), digits = "Product Unit of Measure", required = True)
    currency_id = fields.Many2one(related="purchaserequest_id.currency_id", store=True, string= _("Currency"), readonly=True)

    @api.onchange("product_id")
    def on_product_id_changed(self):
        self.price_unit = self.product_id.standard_price
