from odoo import fields, models, api, exceptions
from odoo.tools.translate import _

class CreateRFQ(models.TransientModel):
    _name = "purchasef99.createrfq"
    _description = "Create Request of Quotation Wizard"

    purchaserequest_id = fields.Many2one("purchasef99.purchaserequest", readonly = True)
    vendor_id = fields.Many2one("res.partner", string = _("Vendor"), required = True)
    product_ids = fields.Many2many("product.product", compute = "_compute_product_ids", readonly = True)
    product_line_ids = fields.One2many("purchasef99.createrfq.line", inverse_name = "create_rfq_id" , string = _("Products"))
    currency_id = fields.Many2one(related = "purchaserequest_id.currency_id", readonly = True)
    date_planned = fields.Datetime(related = "purchaserequest_id.delivery_datetime", readonly = True)
    @api.depends("purchaserequest_id")
    def _compute_product_ids(self):
        for record in self:
            if record.purchaserequest_id:
                record.product_ids = record.purchaserequest_id.product_line_ids.mapped("product_id")
            else:
                record.product_ids = False

    def create_rfq(self):
        rfq = self.env["purchase.order"].create({
            "partner_id": self.vendor_id.id,
            "date_order": fields.datetime.now(),
            "company_id": self.purchaserequest_id.company_id.id,
            "currency_id": self.purchaserequest_id.currency_id.id,
            "name": "New"
        })
        rfq_line_ids = self.env["purchase.order.line"].create([
            {
                "order_id": rfq.id,
                "name": product_line.name,
                "price_unit": product_line.price_unit,
                "product_id": product_line.product_id.id,
                "product_qty": product_line.product_qty,
                "product_uom": product_line.product_uom.id,
                "date_planned": self.date_planned
            }
            for product_line in self.product_line_ids
        ])

        rfq.order_line = rfq_line_ids.ids
        self.purchaserequest_id.rfq_ids |= rfq

        return {
            "type": "ir.actions.client",
            "tag": "close_dialog_and_refresh"
        }
class RFQProductLine(models.TransientModel):
    _name = "purchasef99.createrfq.line"
    _description = _("Purchase Request Line")

    name = fields.Char(related = "product_id.name")
    create_rfq_id = fields.Many2one("purchasef99.createrfq")
    product_ids = fields.Many2many(related = "create_rfq_id.product_ids")
    product_id = fields.Many2one("product.product", string = _("Product"), domain = '[("purchase_ok", "=", True), ("id", "in", product_ids)]')
    product_uom_category_id = fields.Many2one(related='product_id.uom_id.category_id')
    product_uom = fields.Many2one('uom.uom', string= _('Unit of Measure'), domain="[('category_id', '=', product_uom_category_id)]", required = True)
    price_unit = fields.Monetary(string = _("Unit Price"), required = True, digits = "Product Price")
    product_qty = fields.Float(string = _("Quantity"), digits = "Product Unit of Measure", required = True)
    currency_id = fields.Many2one(related="create_rfq_id.currency_id", string= _("Currency"), readonly=True)

    @api.onchange("product_id")
    def on_product_id_changed(self):
        self.price_unit = self.product_id.standard_price
