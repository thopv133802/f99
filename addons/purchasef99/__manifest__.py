{
    "name": "F99 Purchase extension",
    "summary": "F99 Purchase extension",
    "description": "F99 Purchase extension",
    "author": "DTQ",
    "version": "14.0.0.1",
    "application": True,
    "category": "Base",
    "depends": [
        "stock", "hr", "purchase", "product", "web", "mail", "base"
    ],
    "data": [
        "securities/groups.xml",
        "securities/ir.model.access.csv",
        "securities/record_rules.xml",
        "views/assets.xml",
        "views/actions.xml",
        "views/menus.xml",
        "views/purchase_request.xml",
        "wizard/wizards.xml",
        "data/data.xml"
    ],
    "qweb": [

    ]
}