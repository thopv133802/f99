{
    "name": "F99 Base Module",
    "summary": "F99 Base Module",
    "description": "F99 Base customization",
    "author": "DTQ",
    "version": "14.0.0.1",
    "application": True,
    "category": "Base",
    "depends": [
        "web", "mail", "base"
    ],
    "data": [
        "views/assets.xml",
    ],
    "qweb": [
        "static/src/xml/*.xml",
    ]
}