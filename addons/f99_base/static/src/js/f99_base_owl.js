odoo.define("f99_base/static/src/js/f99_base_owl.js", function (require) {
    "use strict"
    const AbstractMessage = require("mail/static/src/components/message/message.js")
    const AbstractMessageList = require('mail/static/src/components/message_list/message_list.js')

    class Message extends AbstractMessage {
        constructor() {
            super(...arguments)
            this.message_removable = this.env.session.is_admin || this.message.author.id === this.env.session.partner_id
            this.message_editable = this.message.message_type === "comment" && (this.env.session.is_admin || this.message.author.id === this.env.session.partner_id)
        }
        async editMessage(new_content) {
            return await this.env.services.rpc({
                model: "mail.message",
                method: "write",
                args: [
                    [this.message.id],
                    {
                        body: new_content
                    }
                ]
            })
        }
        async onEditButtonClicked(event) {
            event.preventDefault()
            $(event.target).attr("disabled", "disabled")
            let $body = $(this.el).find(".o_Message_prettyBody");
            $body.hide()
            const $input = $(`
                <div class = "d-flex flex-column" style = "margin-left: 62px; margin-right: 8px">
                    <textarea autofocus placeholder="Message content goes here..." class="o_ComposerTextInput_textarea o_ComposerTextInput_textareaStyle f99-base-edit-message-textarea" style="height: 80px; width: 100%; background-color: #f2f4f5; margin-bottom: 16px;" >${this.message.body}</textarea>
                    <div class = "d-flex flex-row">
                        <button class = "btn btn-primary f99-base-edit-message-btn-done">Done</button>
                        <button class = "btn btn-secondary f99-base-edit-message-btn-cancel">Cancel</button>
                    </div>
                </div>
            `)
            $input.on("click", ".f99-base-edit-message-btn-done", async (event) => {
                const new_content = $input.find(".f99-base-edit-message-textarea").val();
                const is_ok = await this.editMessage(new_content)
                if(is_ok) {
                    $body.html(new_content)
                    $input.remove()
                    $body.show()
                }
                else {
                    console.log("Edit mail message failed.")
                }
            })
            $input.on("click", ".f99-base-edit-message-btn-cancel", (event) => {
                $input.remove()
                $body.show()
            })
            $input.insertAfter($(this.el))
            $(event.target).removeAttr("disabled")
        }

        async onRemoveButtonClicked(event) {
            event.preventDefault()
            $(event.target).attr("disabled", "disabled")
            const is_ok = await this.env.services.rpc({
                model: "mail.message",
                method: "unlink",
                args: [
                    [this.message.id],
                ]
            })
            if (!is_ok) {
                $(event.target).removeAttr("disabled")
                return
            }

            await this.removeAttachments()
            this.unmount()
            this.destroy()
        }

        async removeAttachments() {
            if (!this.message.attachments || this.message.attachments.length === 0) return

            return await this.env.services.rpc({
                model: "ir.attachment",
                method: "unlink",
                args: [
                    this.message.attachments.map((attachment) => attachment.id)
                ]
            })
        }
    }

    AbstractMessageList.components.Message = Message
})